import useMapboxGeocoding from "../../hooks/useMapboxGeocoding";
import { renderHook, act } from "@testing-library/react-hooks";
import { isEmpty } from "lodash";

test("CheckInitState", () => {
  const { result } = renderHook(() => useMapboxGeocoding());
  const localPlace = result.current.localePlace;
  expect(isEmpty(localPlace)).toBe(true);
});

test("ChecksetState", () => {
  const { result } = renderHook(() => useMapboxGeocoding());
  act(() => result.current.setLocalePlace({ test: "test" }));
  const localPlace = result.current.localePlace;
  expect(localPlace).toStrictEqual({ test: "test" });
});

// test("CheckInitState", () => {
//   const { result } = renderHook(() => useMapboxGeocoding());
//   act(() => result.current.callMaboxGeocoding("Paris"));
//   const localPlace = result.current.localePlace;
//   expect(true).toBe(true);
// });
