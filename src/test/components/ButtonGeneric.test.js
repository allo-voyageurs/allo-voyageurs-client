import ButtonGeneric from "../../components/ButtonGeneric/ButtonGeneric";
import { render, screen } from "@testing-library/react";
import React from "react";

/* Check if the button get correctly the name value */
test("TestValueButton", () => {
  render(<ButtonGeneric name={"hello"} />);
  const name = screen.getByText("hello");
  expect(name).toBeInTheDocument();
  expect(name).not.toBeNull();
});

test("StyleOfButton", () => {
  render(
    <ButtonGeneric
      name={"hello"}
      backgroundColor={"#FAF3F0"}
      borderRadius={"50px"}
      borderColor={"#EABF9F"}
    />
  );

  const genericButton = screen.getByText("hello");

  expect(genericButton).toHaveStyle({
    borderRadius: "50px",
    borderColor: "#EABF9F",
    backgroundColor: "#FAF3F0",
  });
});
