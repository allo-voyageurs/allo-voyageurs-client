import React from "react";
import { Marker } from "react-map-gl";
import "./spotStyle.css";
// import { Popup } from "react-map-gl";
// import ButtonGeneric from "../ButtonGeneric/ButtonGeneric";
// import { useHistory } from "react-router-dom";

const SpotsMarkerMap = ({ spotsData }) => {
  // const history = useHistory();
  // const chat = (idUser2) => {
  //   history.push(`/chat/${idUser2}`);
  // };

  return (
    <>
      {spotsData.map((spot) => (
        <div key={`parrent${spot.id}`}>
          {/* <Popup latitude={spot.lat} longitude={spot.lng}>
            test
          </Popup> */}
          <Marker
            key={`Marker${spot.id}`}
            latitude={spot.lat}
            longitude={spot.lng}
            offsetLeft={-68}
            offsetTop={-150}
          >
            <div key={`div${spot.id}`} className="avatar avatar_rect">
              <p className="spotTitle">{spot.title}</p>
              <img
                key={`img${spot.id}`}
                style={{ width: "50px" }}
                src={spot.adresse}
                alt={spot.title}
              />
            </div>
          </Marker>
        </div>
      ))}
    </>
  );
};

export default SpotsMarkerMap;
