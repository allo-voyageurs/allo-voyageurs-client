import React from "react";
import ReactMapGL, { Source, Layer } from "react-map-gl";
import * as turf from "@turf/turf";

const TestCircleMap = () => {
  //   const lat = 44.837856;
  //   const lng = -0.585156;
  let lng = -0.585156;
  let lat = 44.837856;
  var radius = 5;
  var center = [lng, lat];
  var options = { steps: 50, units: "kilometers", properties: { foo: "bar" } };
  var circle = turf.circle(center, radius, options);
  // var radius = 5;
  // const mapRef = useRef(null);
  var line = turf.lineString(...circle.geometry.coordinates);

  const [viewport, setViewport] = React.useState({
    longitude: lng,
    latitude: lat,
    zoom: 10,
  });

  return (
    <ReactMapGL
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_KEY}
      mapStyle={process.env.REACT_APP_MAPBOX_STYLE}
      {...viewport}
      width="100vw"
      height="100vh"
      onViewportChange={setViewport}
    >
      <div style={{ position: "absolute", bottom: 200, left: 100 }}></div>
      <Source id="my-data" type="geojson" data={circle}>
        <Layer
          id="point-90-hi"
          type="fill"
          paint={{
            "fill-color": "#088",
            "fill-opacity": 0.1,
          }}
        />
      </Source>

      <Source id="my-ata" type="geojson" data={line}>
        <Layer
          id="point-9-hi"
          type="line"
          paint={{
            "line-color": "#3F3979",
            "line-width": 2,
          }}
        />
      </Source>
    </ReactMapGL>
  );
};

export default TestCircleMap;
