import React, { useEffect, useState, useMemo, useRef } from "react";
import useGeoLocation from "../../hooks/useGeoLocation";
import useLocationSocket from "../../hooks/useLocationSocket";
import useUsersStore from "../../hooks/useUsersStore";
import useSpotsStore from "../../hooks/useSpotsStore";
import useMapboxGeocoding from "../../hooks/useMapboxGeocoding";
import useViewportMap from "../../hooks/useViewportMap";
import ReactMapGL from "react-map-gl";
// import ReactMapGL, { GeolocateControl, Layer } from "react-map-gl";
import TravelersMarkersMap from "./TravelersMarkersMap";
import SpotsMarkerMap from "./SpotsMarkerMap";
import DetectionCircle from "./DetectionCircle";
import BubbleMap from "./BubbleMap";
import SearchBar from "../SearchBar/SearchBar";
// import RadiusBar from "../RadiusBar/RadiusBar";
import SlideButton from "../SlideButton/SlideButton";
import { isEmpty } from "lodash";

import "react-map-gl-geocoder/dist/mapbox-gl-geocoder.css";
import "mapbox-gl/dist/mapbox-gl.css";
import mapboxgl from "!mapbox-gl";

import "./style.css";

const Map = ({ radiusDetection, setRadiusDetection }) => {
  const [loaded, setLoaded] = useState(false);
  const [selectedTraveler, setSelectedTraveler] = useState(null);

  const [isSelectedEntraide, setIsSelectedEntraide] = useState(true);
  const { viewport, setViewport, handleViewportChange } = useViewportMap();
  const { location, callLocation } = useGeoLocation();
  const { callMaboxGeocoding, localePlace, setLocalePlace } =
    useMapboxGeocoding();
  const { change, EmitUserLocation, userBlocksGeo } = useLocationSocket();
  const {
    travelersData,
    searchTravelersAround,
    updateUserLocation,
    removeUserNotAuthorize,
  } = useUsersStore();

  const { spotsData, searchSpotsAround } = useSpotsStore();

  const user = useMemo(() => JSON.parse(localStorage.getItem("profile")), []);
  const mapRef = useRef();

  useEffect(() => {
    const listener = (e) => {
      if (e.key === "Escape") setSelectedTraveler(null);
    };
    window.addEventListener("keydown", listener);
    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, [setSelectedTraveler]);

  useEffect(() => {
    callLocation();
  }, [callLocation]);

  useEffect(() => {
    if (location.loaded && loaded) {
      EmitUserLocation({
        id: user && user.result.id,
        lat: location.coordinates.lat,
        lng: location.coordinates.lng,
      });
    }
  }, [location, loaded, user, EmitUserLocation]);

  useEffect(() => {
    if (location.loaded && loaded) {
      searchTravelersAround(
        location.coordinates,
        user && user.result.id,
        radiusDetection * 1000.0
      );
      searchSpotsAround(location.coordinates, radiusDetection * 1000.0);

      setViewport((vp) => ({
        ...vp,
        latitude: location.coordinates.lat,
        longitude: location.coordinates.lng,
        transitionDuration: 2000,
      }));
    }
  }, [
    location,
    loaded,
    user,
    radiusDetection,
    EmitUserLocation,
    searchTravelersAround,
    setViewport,
    searchSpotsAround,
  ]);

  useEffect(() => {
    updateUserLocation(change);
  }, [change, updateUserLocation]);

  useEffect(() => {
    var zoom = 10;
    switch (radiusDetection) {
      case 10:
        zoom = 10;
        break;
      case 15:
        zoom = 9;
        break;
      case 20:
        zoom = 8.5;
        break;
      default:
        zoom = 11;
    }
    setViewport((vp) => ({
      ...vp,
      zoom,
    }));
  }, [radiusDetection, setViewport]);

  useEffect(() => {
    if (!isEmpty(localePlace)) {
      searchTravelersAround(
        {
          lat: localePlace.coordinates[1],
          lng: localePlace.coordinates[0],
        },
        null,
        radiusDetection * 1000
      );
      searchSpotsAround(
        {
          lat: localePlace.coordinates[1],
          lng: localePlace.coordinates[0],
        },
        radiusDetection * 1000.0
      );
      setViewport((vp) => ({
        ...vp,
        latitude: localePlace.coordinates[1],
        longitude: localePlace.coordinates[0],
        transitionDuration: 1000,
      }));
    } else {
      searchTravelersAround(
        location.coordinates,
        user && user.result.id,
        radiusDetection * 1000
      );
      searchSpotsAround(location.coordinates, radiusDetection * 1000.0);
      setViewport((vp) => ({
        ...vp,
        latitude: location.coordinates.lat,
        longitude: location.coordinates.lng,
        transitionDuration: 1000,
      }));
    }
  }, [
    localePlace,
    setViewport,
    searchSpotsAround,
    location,
    searchTravelersAround,
    user,
    radiusDetection,
  ]);

  useEffect(() => {
    if (!isEmpty(userBlocksGeo)) {
      userBlocksGeo.forEach((userToBlock) => {
        removeUserNotAuthorize(userToBlock);
      });
    }
  }, [userBlocksGeo, removeUserNotAuthorize]);

  useEffect(() => {
    setViewport((vp) => ({
      ...vp,
      pitch: isSelectedEntraide ? 0 : 65,
      transitionDuration: 1000,
    }));
  }, [isSelectedEntraide, setViewport]);

  return (
    <>
      <ReactMapGL
        {...viewport}
        ref={mapRef}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_KEY}
        mapStyle={process.env.REACT_APP_MAPBOX_STYLE}
        onViewportChange={handleViewportChange}
        onLoad={() => setLoaded(true)}
      >
        {/* <GeolocateControl
          style={geolocateControlStyle}
          positionOptions={{ enableHighAccuracy: true }}
          trackUserLocation={true}
          auto
        /> */}

        {isSelectedEntraide ? (
          <>
            {" "}
            <TravelersMarkersMap
              travelersData={travelersData}
              user={user}
              setSelectedTraveler={setSelectedTraveler}

            />
            <BubbleMap
              selectedTraveler={selectedTraveler}
              user={user}
              setSelectedTraveler={setSelectedTraveler}
            />
          </>
        ) : (
          <>
            <SpotsMarkerMap
              spotsData={spotsData}
            />
          </>
        )}

        <DetectionCircle
          lat={
            !isEmpty(localePlace)
              ? localePlace.coordinates[1]
              : location.coordinates.lat
          }
          lng={
            !isEmpty(localePlace)
              ? localePlace.coordinates[0]
              : location.coordinates.lng

          }
          radius={radiusDetection}
        />

        <SearchBar
          callMaboxGeocoding={callMaboxGeocoding}
          setLocalePlace={setLocalePlace}
        />

        <SlideButton
          setIsSelectedEntraide={setIsSelectedEntraide}
/>
      </ReactMapGL>
    </>
  );
};

export default Map;
