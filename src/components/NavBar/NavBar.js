import { useState, useRef, useEffect } from "react";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import PhotoCameraIcon from "@mui/icons-material/PhotoCamera";
import AirportShuttleIcon from "@mui/icons-material/AirportShuttle";
import RoomIcon from "@mui/icons-material/Room";
import PersonIcon from "@mui/icons-material/Person";
import Paper from "@mui/material/Paper";
import { useHistory } from "react-router-dom";
import IconeBadge from "../IconeBadge/IconeBadge";
import useChatSocket from "../../hooks/useChatSocket";
import { useDispatch } from "react-redux";
import { updateMessageNotRead } from "../../actions/auth";
import { CountNbBadge } from "../../utils/CountNbBadge";
import { isEmpty } from "lodash";
import "./style.css";

const NavBar = () => {
  const [value, setValue] = useState(0);
  const ref = useRef(null);
  const user = JSON.parse(localStorage.getItem("profile"));
  const localNbBadge = user ? CountNbBadge(user.result.noReadMessage) : 0;
  console.log(localNbBadge);
  const { messageNoRead } = useChatSocket();
  const [nbBadge, setNbBadge] = useState(0);

  const history = useHistory();
  const dispatch = useDispatch();

  const router = (path) => {
    history.push(path);
  };

  useEffect(() => {
    if (!isEmpty(messageNoRead)) {
      setNbBadge(CountNbBadge(messageNoRead));
      console.log(messageNoRead);
    }
    if (!isEmpty(messageNoRead)) {
      console.log(messageNoRead);
      dispatch(updateMessageNotRead(messageNoRead));
    }
  }, [messageNoRead, dispatch]);

  return (
    <Box sx={{ pb: 7 }} ref={ref}>
      <Paper
        sx={{ position: "fixed", bottom: 0, left: 0, right: 0 }}
        elevation={3}
      >
        <BottomNavigation
          /*           showLabels
           */ value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          style={{ backgroundColor: "#3F3979" }}
          // focused={true}
        >
          <BottomNavigationAction
            style={{ color: "#ffffff" }}
            onClick={() => router("/map")}
            icon={<RoomIcon />}
          />
          <BottomNavigationAction
            style={{ color: "#ffffff" }}
            onClick={() => router("/spot")}
            icon={<PhotoCameraIcon />}
          />
          <BottomNavigationAction
            style={{ color: "#ffffff" }}
            onClick={() => router("/chats")}
            icon={
              <IconeBadge
                iconeName={"ForumIcon"}
                badgeNb={user ? localNbBadge : nbBadge}
              />
            }
          />
          <BottomNavigationAction
            style={{ color: "#ffffff" }}
            onClick={() => router("/ad")}
            icon={<AirportShuttleIcon />}
          />
          <BottomNavigationAction
            style={{ color: "#ffffff" }}
            onClick={() => router("/profil")}
            icon={<PersonIcon />}
          />
        </BottomNavigation>
      </Paper>
    </Box>
  );
};
export default NavBar;
