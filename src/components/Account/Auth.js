import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { signin, signup } from "../../actions/auth";
import Avatar from "@mui/material/Avatar";
import Container from "@mui/material/Container";

import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import "./style.css";

// const initialState = {
//   pseudo: "",
//   email: "",
//   password: "",
//   confirmPassword: "",
//   phone: "",
//   role: "",
// };

const initialState = {
  first_name: "",
  last_name: "",
  email: "",
  password: "",
  confirmPassword: "",
  phone: "",
  role: "",
};

const Auth = () => {
  const [form, setForm] = useState(initialState);
  const [isSignup, setIsSignup] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();

  const switchMode = () => {
    setForm(initialState);
    setIsSignup((prevIsSignup) => !prevIsSignup);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (isSignup) {
      dispatch(signup(form, history));
    } else {
      dispatch(signin(form, history));
    }
  };

  const handleChange = (e) =>
    setForm({ ...form, [e.target.name]: e.target.value });

  useEffect(() => {
    console.log(form);
  }, [form]);

  const useStyles = makeStyles((theme) =>
    createStyles({
      messageRow: {
        display: "flex",
        placeItems: "end",
        marginBottom: "30px",
        marginTop: "20px",
      },

      messageWhite: {
        position: "relative",
        marginLeft: "10px",
        padding: "13px 15px 15px 15px",
        backgroundColor: "white",
        textAlign: "left",
        borderRadius: "12px 12px 12px 0",
      },

      messageContent: {
        padding: 0,
        margin: 0,
        fontSize: "18px",
        color: "#3F3979",
      },

      labelInput: {
        margin: "0 0 5px 0",
        color: "#3F3979",
      },
    })
  );

  const classes = useStyles();

  return (
    <Container fixed style={{ height: "100vh", padding: "20px" }}>
      <div>
        <IconButton
          style={{
            backgroundColor: "#A480A6",
            color: "white",
            borderRadius: "8px",
            marginBottom: "15px",
          }}
          onClick={history.goBack}
        >
          <ChevronLeftIcon fontSize="large" />
        </IconButton>
      </div>

      <div className={classes.messageRow}>
        <Avatar style={{ backgroundColor: "white" }} />
        <div className={classes.messageWhite}>
          <p className={classes.messageContent}>
            {isSignup
              ? "Crée ton compte pour pouvoir demander de l'aide, enregistrer et créer des annonces !"
              : "Connecte toi à ton compte pour pouvoir profiter de Allô Voyageur à son maximum !"}
          </p>
        </div>
      </div>

      <form
        onSubmit={handleSubmit}
        style={{ width: "100vw", color: "black !important" }}
      >
        <section>
          <div>
            {isSignup && (
              <>
                {/* <p className={classes.labelInput}>Rentre ton pseudo</p>
                <TextField
                  style={{ marginBottom: "15px" }}
                  required
                  id="lName"
                  name="pseudo"
                  onChange={handleChange}
                  size="small"
                /> */}

                <p className={classes.labelInput}>Rentre ton prénom</p>
                <TextField
                  style={{ marginBottom: "15px" }}
                  required
                  id="first_name"
                  name="first_name"
                  onChange={handleChange}
                  size="small"
                />

                <p className={classes.labelInput}>Rentre ton nom</p>
                <TextField
                  style={{ marginBottom: "15px" }}
                  required
                  id="last_name"
                  name="last_name"
                  onChange={handleChange}
                  size="small"
                />
              </>
            )}
            <p className={classes.labelInput}>Rentre ton email</p>
            <TextField
              style={{ marginBottom: "15px" }}
              required
              id="email"
              name="email"
              onChange={handleChange}
              size="small"
            />

            <p className={classes.labelInput}>Rentre ton mot de passe</p>
            <TextField
              style={{ marginBottom: "4px" }}
              required
              id="password"
              type="password"
              name="password"
              size="small"
              onChange={handleChange}
            />
            <p style={{ color: "#3F3979", margin: "0", fontStyle: "italic" }}>
              {isSignup ? " " : "Mot de passe oublié"}
            </p>

            {isSignup && (
              <>
                <p className={classes.labelInput}>Confirme ton mot de passe</p>
                <TextField
                  style={{ marginBottom: "15px" }}
                  id="confirmed_password"
                  type="password"
                  name="confirmPassword"
                  size="small"
                  onChange={handleChange}
                />
              </>
            )}
          </div>

          <div>
            <Button
              variant="contained"
              type="submit"
              sx={{
                borderRadius: 2,
                backgroundColor: "#EBA701",
                py: 1.5,
                color: "white",
                fontSize: "20px",
                fontWeight: "400",
                boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.2)",
                marginTop: "30px",
              }}
            >
              {isSignup ? "Valider" : "Connexion"}
            </Button>
          </div>
        </section>
      </form>
      <Button
        onClick={switchMode}
        variant="text"
        size="medium"
        type="button"
        style={{ color: "#3F3979", textTransform: "none", fontSize: "16px" }}
      >
        {isSignup ? "Je me connecte" : "Je n'ai pas encore de compte ?"}
      </Button>
    </Container>
  );
};

export default Auth;
