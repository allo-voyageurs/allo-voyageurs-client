import React from "react";
import Badge from "@mui/material/Badge";
import Stack from "@mui/material/Stack";
import ForumIcon from "@mui/icons-material/Forum";

const IconeBadge = ({ iconeName, badgeNb }) => {
  var icons = [];
  icons["ForumIcon"] = <ForumIcon style={{ color: "#ffffff" }} />;

  return (
    <div>
      <Stack spacing={2} direction="row">
        <Badge badgeContent={badgeNb} color="secondary">
          {icons[iconeName]}
        </Badge>
      </Stack>
    </div>
  );
};

export default IconeBadge;
