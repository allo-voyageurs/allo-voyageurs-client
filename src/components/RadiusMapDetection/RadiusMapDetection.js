import * as React from "react";
import Box from "@mui/material/Box";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import List from "@mui/material/List";
import SocialDistanceIcon from "@mui/icons-material/SocialDistance";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

import Fab from "@mui/material/Fab";

const RadiusMapDetection = ({ setRadiusDetection, radiusDetection }) => {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <FormControl component="fieldset">
        <FormLabel component="legend">Rayon : </FormLabel>
        <RadioGroup
          aria-label="gender"
          name="radio-buttons-group"
          // defaultValue={radiusDetection}
        >
          <List>
            {[5, 10, 15, 20].map((km, index) => (
              <FormControlLabel
                key={index}
                value={km}
                control={
                  <Radio
                    checked={km === radiusDetection}
                    onClick={() => setRadiusDetection(km)}
                  />
                }
                label={`${km} km`}
              />
            ))}
          </List>
        </RadioGroup>
      </FormControl>
    </Box>
  );

  const fabStyle = {
    position: "absolute",
    bottom: 90,
    right: 30,
  };

  return (
    <>
      <Fab
        sx={fabStyle}
        aria-label={"Add"}
        color={"blue"}
        onClick={toggleDrawer("bottom", true)}
      >
        <SocialDistanceIcon />
      </Fab>

      <SwipeableDrawer
        anchor={"bottom"}
        open={state["bottom"]}
        onClose={toggleDrawer("bottom", false)}
        onOpen={toggleDrawer("bottom", true)}
      >
        {list("bottom")}
      </SwipeableDrawer>
    </>
  );
};

export default RadiusMapDetection;
