import React, { useState } from "react";
import Axios from "axios";
import Dropzone from "react-dropzone";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import SpotService from "../../services/SpotService";

import { useHistory } from "react-router-dom";
import { createStyles, makeStyles } from "@material-ui/core/styles";

const Spot2 = () => {
  const [imageSelected, setImageSelected] = useState("");
  const [form, setForm] = useState();
  const history = useHistory();

  const uploadImage = () => {
    const formData = new FormData();
    formData.append("file", imageSelected);
    formData.append("upload_preset", "traveler-spot");

    Axios.post(
      "https://api.cloudinary.com/v1_1/db00tntyg/image/upload",
      formData
    ).then((response) => {
      SpotService.CreateSpot(form, response.data.url).then(() => {
        history.push("/map");
      });
      return response.data.url;
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    uploadImage();
  };

  const handleChange = (e) =>
    setForm({ ...form, [e.target.name]: e.target.value });

  const useStyles = makeStyles((theme) =>
    createStyles({
      labelInput: {
        margin: "0 0 5px 0",
        color: "#3F3979",
      },
    })
  );
  const classes = useStyles();

  return (
    <div>
      <Dropzone onDrop={(acceptedFiles) => setImageSelected(acceptedFiles[0])}>
        {({ getRootProps, getInputProps }) => (
          <section>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <Button
                variant="contained"
                style={{
                  marginTop: "30px",
                  marginBottom: "15px",
                  backgroundColor: "#EBA701",
                  textTransform: "none",
                  fontSize: "16px",
                  boxShadow: "none",
                }}
              >
                Import une image
              </Button>
            </div>
          </section>
        )}
      </Dropzone>
      {/* <Button onClick={uploadImage} variant="contained">
        Create Spot
      </Button> */}

      <form onSubmit={handleSubmit}>
        <section className="accFormWrapper">
          <div className="alignCenter">
            <p className={classes.labelInput}>Rentre un titre</p>
            <TextField
              style={{ marginBottom: "15px" }}
              required
              id="title"
              name="title"
              onChange={handleChange}
              size="small"
            />
            <p className={classes.labelInput}>Rentre une description</p>
            <TextField
              style={{ marginBottom: "15px" }}
              required
              id="description"
              name="description"
              onChange={handleChange}
              size="small"
            />
            <p className={classes.labelInput}>Rentre une latitude</p>
            <TextField
              style={{ marginBottom: "15px" }}
              required
              id="lat"
              name="lat"
              onChange={handleChange}
              size="small"
            />
            <p className={classes.labelInput}>Rentre une longitude</p>
            <TextField
              style={{ marginBottom: "15px" }}
              id="lng"
              name="lng"
              onChange={handleChange}
              size="small"
            />
          </div>
          <div className="alignCenter">
            <Button
              variant="contained"
              type="submit"
              sx={{
                borderRadius: 2,
                backgroundColor: "#EBA701",
                py: 1.5,
                color: "white",
                fontSize: "16px",
                fontWeight: "400",
                boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.2)",
                marginTop: "30px",
              }}
            >
              Add Spots
            </Button>
          </div>
        </section>
      </form>
    </div>
  );
};

export default Spot2;
