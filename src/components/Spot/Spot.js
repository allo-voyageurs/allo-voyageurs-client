import React, { useState, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import Button from "@mui/material/Button";

const Spot = () => {
  const [image] = useState(null);

  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      const reader = new FileReader();

      reader.onabort = () => console.log("file reading was aborted");
      reader.onerror = () => console.log("file reading has failed");
      reader.onload = () => {
        // Do whatever you want with the file contents
        const binaryStr = reader.result;
        console.log(binaryStr);
        //   fetch('http://server.com/api/upload', {
        //     method: 'POST',
        //     body: binaryStr
        //   });
      };
      reader.readAsArrayBuffer(file);
    });
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <>
      <div {...getRootProps()}>
        <input {...getInputProps()} />
        <Button variant="contained">Upload</Button>
      </div>
      {/* <Dropzone onDrop={(acceptedFiles) => handleFile(acceptedFiles)}>
        {({ getRootProps, getInputProps }) => (
          <section>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <p>Drag 'n' drop some files here, or click to select files</p>
            </div>
          </section>
        )}
      </Dropzone> */}

      {image && <img src={image} alt={image.path} />}
    </>
  );
};

export default Spot;
