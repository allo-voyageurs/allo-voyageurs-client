import React, { useState } from "react";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import "./style.css";

const SlideButton = ({ setIsSelectedEntraide }) => {
  const [alignment, setAlignment] = useState("Entraide");

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  return (
    <ToggleButtonGroup
      // color="primary"
      value={alignment}
      exclusive
      onChange={handleChange}
      style={{
        backgroundColor: "white",
        borderRadius: "8px",
        marginLeft: "20px",
        marginRight: "20px",
      }}
      fullWidth={true}
    >
      <ToggleButton
        value="Entraide"
        onClick={() => setIsSelectedEntraide(true)}
        style={{
          textTransform: "none",
          lineHeight: "1",
          border: "0",
          color: "#A480A6",
        }}
      >
        Entraide
      </ToggleButton>
      <ToggleButton
        value="Spots"
        onClick={() => setIsSelectedEntraide(false)}
        style={{
          textTransform: "none",
          lineHeight: "1",
          border: "0",
          color: "#A480A6",
        }}
      >
        Spots
      </ToggleButton>
    </ToggleButtonGroup>
  );
};

export default SlideButton;
