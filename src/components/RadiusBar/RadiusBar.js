import React, { useCallback } from "react";
import Paper from "@mui/material/Paper";
import Slider from "@mui/material/Slider";
import "./radiusBar.css";

const RadiusBar = ({ setRadiusDetection }) => {
  const marks = [
    {
      value: 5,
      label: "5 km",
    },
    {
      value: 10,
      label: "10 km",
    },
    {
      value: 15,
      label: "15 km",
    },
    {
      value: 20,
      label: "20 km",
    },
  ];
  const test = useCallback(
    (current) => {
      setRadiusDetection(current);
    },
    [setRadiusDetection]
  );

  return (
    <Paper
      sx={{
        p: "5px 5px",
        display: "flex",
        alignItems: "center",
        height: "30px",
        borderRadius: "16px",
        margin: "20px 70px 15px 70px",
        boxShadow: "none",
        bottom: "0 !important",
      }}
    >
      <Slider
        aria-label="Small steps"
        defaultValue={5}
        getAriaValueText={test}
        step={5}
        min={5}
        max={20}
        marks={marks}
        valueLabelDisplay="auto"
        sx={{
          margin: "20px 30px 15px 30px",
        }}
      />
    </Paper>
  );
};

export default RadiusBar;
