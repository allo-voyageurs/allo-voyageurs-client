import React, { useEffect, useState, useMemo } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import chatService from "../../services/ChatService";
import { useHistory } from "react-router-dom";
import { isEmpty } from "lodash";

const AllChats = () => {
  const user = useMemo(() => JSON.parse(localStorage.getItem("profile")), []);
  const [chats, setChats] = useState([]);
  const history = useHistory();
  const goChat = (idUser2) => {
    history.push(`/chat/${idUser2}`);
  };

  useEffect(() => {
    if (user) {
      chatService.getAllChats(user.result.id).then((chatsData) => {
        console.log(chatsData.data);
        setChats(chatsData.data);
      });
    }
  }, [user]);

  return (
    <List sx={{ width: "100%", maxWidth: 360 }}>
      {!isEmpty(chats) ? (
        chats.map(
          (chat) =>
            !isEmpty(chat.message) && (
              <React.Fragment key={`Fragment${chat.id}`}>
                <ListItem
                  onClick={() => goChat(chat.message[0].user.id)}
                  key={`ListItem${chat.id}`}
                >
                  <ListItemAvatar key={`ListItemAvatar${chat.id}`}>
                    <Avatar key={`Avatar${chat.id}`}>
                      {/* {chat.message[0].user.pseudo.charAt(0)} */}
                      {chat.message[0].user.first_name.charAt(0)}
                      {chat.message[0].user.last_name.charAt(0)}
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    key={`ListItemText${chat.id}`}
                    // primary={chat.message[0].user.pseudo.charAt(0)}
                    primary={chat.message[0].user.first_name}
                    secondary={chat.message[0].user.last_name}
                  />
                  <MoreHorizIcon key={`MoreHorizIcon${chat.id}`} />
                </ListItem>
                <Divider
                  key={`Divider${chat.id}`}
                  variant="inset"
                  component="li"
                />
              </React.Fragment>
            )
        )
      ) : (
        <p style={{ fontSize: "18px", color: "#3F3979", fontWeight: "bold" }}>
          Tu n'as actuellement aucunes discussions !
        </p>
      )}
    </List>
  );
};

export default AllChats;
