import React from 'react'
import { createStyles, makeStyles } from "@material-ui/core/styles";
import SendIcon from '@material-ui/icons/Send';
import './style.css'
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";


const useStyles = makeStyles((theme) =>
  createStyles({
    wrapForm : {
        display: "flex",
        justifyContent: "center",
        width: "95%",
        margin: `${theme.spacing(0)} auto`
    },
    wrapText  : {
        width: "100%"
    },
    button: {
        //margin: theme.spacing(1),
    },
  })
);

export const TextInput = ({ handleMessageSubmit, handleTextChange, currentMessage }) => {
    const classes = useStyles();
    return (
        <>
            <Paper
                sx={{ p: "2px 6px", display: "flex", alignItems: "center" }}
                style={{
                    height: "50px",
                    borderRadius: "16px",
                    boxShadow: "none",
                    // width: "100%"
                }}
            >
                <InputBase
                    name="message"
                    placeholder="Ecrire mon message"
                    style={{fontSize: "18px", paddingLeft: "10px"}}
                    className={classes.wrapText}
                    value={currentMessage["message"]}
                    onChange={(e) => handleTextChange(e)}>
                </InputBase>
                <IconButton
                    onClick={handleMessageSubmit}
                    style={{
                    backgroundColor: "#EBA701",
                    borderRadius: "8px",
                    color: "white",
                    }}
                >
                    <SendIcon />
                </IconButton>
            </Paper>
        </>
    )
}



