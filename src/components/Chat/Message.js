import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) =>
  createStyles({
    messageRow: {
      display: "flex",
      placeItems: "end",
      marginBottom: "15px",
    },
    messageRowRight: {
      display: "flex",
      justifyContent: "flex-end",
    },
    messagePurple: {
      position: "relative",
      marginLeft: "10px",
      padding: "13px 15px 15px 15px",
      backgroundColor: "#3F3979",
      color: "white",
      textAlign: "left",
      borderRadius: "12px 12px 12px 0",
    },
    messageWhite: {
      position: "relative",
      marginBottom: "15px",
      padding: "13px 15px 15px 15px",
      backgroundColor: "white",
      textAlign: "left",
      borderRadius: "12px 0 12px 12px",
    },

    messageContent: {
      padding: 0,
      margin: 0,
      fontSize: "18px",
    },

    avatarPurple: {
      color: "white",
      backgroundColor: "#3F3979",
      width: theme.spacing(4),
      height: theme.spacing(4),
    },

    //PAS BESOIN POUR LE MOMENT CAR PAS DE TIMESTAMP
    // messageTimeStampRight: {
    //   position: "absolute",
    //   fontSize: ".85em",
    //   fontWeight: "300",
    //   marginTop: "10px",
    //   bottom: "-3px",
    //   right: "5px"
    // },
  })
);

//avatar
export const MessageLeft = (props) => {
  const message = props.message ? props.message : "no message";
  // const timestamp = props.timestamp ? props.timestamp : "";
  const photoURL = props.photoURL ? props.photoURL : "dummy.js";
  const classes = useStyles();
  return (
    <>
      <div className={classes.messageRow}>
        <Avatar
          // alt={displayName}
          className={classes.avatarPurple}
          src={photoURL}
        ></Avatar>
        <div className={classes.messagePurple}>
          <p className={classes.messageContent}>{message}</p>
        </div>
      </div>
      {/* <div className={classes.messageTimeStampRight}>{timestamp}</div> */}
    </>
  );
};
//avatar
export const MessageRight = (props) => {
  const classes = useStyles();
  const message = props.message ? props.message : "no message";
  const timestamp = props.timestamp ? props.timestamp : "";
  return (
    <div className={classes.messageRowRight}>
      <div className={classes.messageWhite}>
        <p className={classes.messageContent}>{message}</p>
        <div className={classes.messageTimeStampRight}>{timestamp}</div>
      </div>
    </div>
  );
};
