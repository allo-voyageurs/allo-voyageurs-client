import React, { useEffect, useMemo } from "react";
import { Container } from "@material-ui/core";
import { TextInput } from "./TextInput.js";
import { MessageLeft, MessageRight } from "./Message";
// import ChatStyle from './ChatStyle';
import IconButton from "@mui/material/IconButton";
import useChatSocket from "../../hooks/useChatSocket";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import "./style.css";
import { useHistory } from "react-router-dom";

export default function ChatUi(user2) {
  // const classes = ChatStyle;
  const { InitChat, EmitMessage, chat, currentMessage, setCurrentMessage } =
    useChatSocket(user2.match.params.idUser2);
  const user = useMemo(() => JSON.parse(localStorage.getItem("profile")), []);

  useEffect(() => {
    InitChat(user.result.id, user2.match.params.idUser2);
  }, [InitChat, user, user2]);

  const handleTextChange = (e) => {
    setCurrentMessage({ ...currentMessage, [e.target.name]: e.target.value });
  };

  const handleMessageSubmit = (e) => {
    if (user) {
      const { message } = currentMessage;
      EmitMessage(
        user.result.first_name,
        message,
        user.result.id,
        user2.match.params.idUser2
      );
      e.preventDefault();
    }
  };
  const history = useHistory();

  return (
    <Container style={{ height: "100vh", width: "100vw", padding: "20px" }}>
      <div>
        <IconButton
          style={{
            backgroundColor: "#A480A6",
            color: "white",
            borderRadius: "8px",
            marginBottom: "15px",
          }}
          onClick={history.goBack}
        >
          <ChevronLeftIcon fontSize="large" />
        </IconButton>
      </div>
      <div>
        {chat.map(({ name, message, userId }, index) => (
          <div key={`div${index}`}>
            {userId === user.result.id ? (
              <MessageRight
                key={index}
                message={message}
                // timestamp="MM/DD 00:00"
                // photoURL=""
                // displayName={name}
                avatarDisp={true}
              />
            ) : (
              <MessageLeft
                key={index}
                message={message}
                // timestamp="MM/DD 00:00"
                // photoURL=""
                // displayName={name}
                avatarDisp={true}
              />
            )}
          </div>
        ))}
      </div>
      <div style={{ placeItems: "flex-end" }}>
        <TextInput
          handleMessageSubmit={handleMessageSubmit}
          handleTextChange={handleTextChange}
          currentMessage={currentMessage}
        />
      </div>
    </Container>
  );
}
