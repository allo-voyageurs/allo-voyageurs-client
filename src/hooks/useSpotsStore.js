import { useState, useCallback } from "react";
import SpotService from "../services/SpotService";

const useSpotsStore = () => {
  const [spotsData, setSpotsData] = useState([]);

  const searchSpotsAround = useCallback((userLocation, radius) => {
    SpotService.findSpotsArround(userLocation, radius).then((spots) => {
      setSpotsData(spots.data);
    });
  }, []);

  return {
    spotsData,
    searchSpotsAround,
  };
};

export default useSpotsStore;
