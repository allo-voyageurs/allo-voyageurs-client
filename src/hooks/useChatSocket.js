import { useEffect, useState, useCallback } from "react";
import chatService from "../services/ChatService";
import { isEmpty } from "lodash";
import io from "socket.io-client";

let socket;
let chatId = null;

const useChatSocket = (recipientId) => {
  const [messageNoRead, setMessageNoRead] = useState([]);
  const [currentMessage, setCurrentMessage] = useState({
    message: "",
    name: "",
  });
  const user = JSON.parse(localStorage.getItem("profile"));
  const [chat, setChat] = useState([]);
  let uriSocketMessage = `messageTo_user${
    user && user.result.id
  }dest${recipientId}`;

  useEffect(() => {
    socket = io(process.env.REACT_APP_URL_API_VOYAGEUR, {
      withCredentials: false,
      extraHeaders: {
        origins: "allowedOrigins",
      },
      enabledTransports: ["websocket", "ws", "wss"],
      transports: ["websocket", "ws", "wss"],
    });

    socket.on(
      uriSocketMessage,
      ({ name, message, userId, chatId, recipientId }) => {
        setChat((cuurent) => {
          console.log(cuurent);
          if (
            user &&
            cuurent &&
            cuurent.at(-1).message === message &&
            userId === user.result.id
          ) {
            return cuurent;
          } else {
            return [...cuurent, { name, message, userId, chatId, recipientId }];
          }
        });
      }
    );

    socket.on(`messageTo${user && user.result.id}`, ({ chatId }) => {
      setMessageNoRead((current) => {
        const myArray = [...current];
        const value = myArray.find((value) => value.idChat === chatId);
        if (value) {
          value.nb += 1;
        } else {
          myArray.push({ idChat: chatId, nb: 1 });
        }
        return myArray;
      });
    });

    socket.on("connect_error", (err) => {
      console.log(`connect_error due to ${err.message}`);
    });
  }, [user, uriSocketMessage]);

  const InitChat = useCallback((idUser1, idUser2) => {
    chatService.getChatMessages(idUser1, idUser2).then((messages) => {
      if (!isEmpty(messages.data)) {
        var messagesArray = [];
        messages.data.forEach((message) => {
          chatId = message.chat_id;
          if (message.content) {
            const mess = {
              name: message.user.first_name,
              message: message.content,
              userId: message.user_id,
              chatId: message.chat_id,
              recipientId: message.recipient_id,
            };
            messagesArray.push(mess);
          }
        });
        setChat(messagesArray);
      } else {
        chatService.createChat().then((chat) => {
          chatId = chat.data.id;
          chatService.postMessages({
            content: null,
            user_id: idUser1,
            chat_id: chat.data.id,
            recipient_id: idUser2,
          });
          chatService.postMessages({
            content: null,
            user_id: idUser2,
            chat_id: chat.data.id,
            recipient_id: idUser1,
          });
        });
      }
    });
  }, []);

  const EmitMessage = useCallback((name, message, userId, recipientId) => {
    socket.emit("SendMessage", {
      name,
      message,
      userId,
      chatId,
      recipientId,
    });
    setCurrentMessage({ message: "", name });
  }, []);

  return {
    InitChat,
    EmitMessage,
    chat,
    chatId,
    currentMessage,
    setCurrentMessage,
    messageNoRead,
  };
};

export default useChatSocket;
