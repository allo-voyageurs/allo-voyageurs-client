import { isEmpty } from "lodash";
export const CountNbBadge = (array) => {
  if (!isEmpty(array)) {
    var nbBadge = 0;
    array.forEach((val) => {
      nbBadge += val.nb;
    });
    return nbBadge;
  }
  return 0;
};
