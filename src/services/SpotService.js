import Axios from "axios";

const API = Axios.create({
  baseURL: `${process.env.REACT_APP_URL_API_VOYAGEUR}/spots_publications`,
});

class SpotService {
  AddCloudPicture(picture) {
    return new Promise((resolve, reject) => {
      const formData = new FormData();
      formData.append("file", picture);
      formData.append("upload_preset", "traveler-spot");

      Axios.post(process.env.REACT_APP_URL_API_PHOTO, formData)
        .then((response) => resolve(response.data.url))
        .catch((err) => reject(err));
    });
  }

  CreateSpot(bodyRequest, picture) {
    const user = JSON.parse(localStorage.getItem("profile"));
    bodyRequest.user_id = user.result.id;
    bodyRequest.adresse = picture;

    return new Promise((resolve, reject) => {
      API.post(`/createspot`, bodyRequest)
        .then((response) => {
          console.log(response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  findSpotsArround(location, radius) {
    return new Promise((resolve, reject) => {
      API.get(`/spotsArround/${location.lat}/${location.lng}/${radius}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
}

const spotService = new SpotService();
export default spotService;
