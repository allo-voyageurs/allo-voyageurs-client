import React from 'react'
import Layout from '../../components/Layout/Layout'// import Grid from '@mui/material/Grid';
import NavBar from '../../components/NavBar/NavBar';
import Spot2 from '../../components/Spot/Spot2';
import Grid from "@mui/material/Grid";

const SpotPage = () => {
    return (
      <>
      <Layout>
        <Grid container padding="20px">
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div>
              <h2 style={{fontSize: "28px", color:"#8F8FD9", margin: '0', fontStyle: "bold"}}>Let's go !</h2>
              <p style={{fontSize: "18px", color:"#3F3979", margin: '10px 0 0 0'}}>Fais découvrir tes pépites aux vanlifers de la communautés !</p>
            </div>
            {/* <SearchBar style={{width: "100vw"}}/> */}
            <Spot2></Spot2>
          </Grid>
        </Grid>

      </Layout>
      <NavBar />
      </>
    )
}

export default SpotPage