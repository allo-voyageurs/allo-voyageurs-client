import React from "react";
import Button from "@mui/material/Button";
import backgroundImage from "../../images/backgroundMobile.jpg";
import { useHistory } from "react-router-dom";
import { Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";

const HomePage = () => {
  //   const classes = useStyles();
  const history = useHistory();
  const map = () => {
    history.push("/map");
  };

  return (
    <Stack
      direction="column"
      justifyContent="space-between"
      sx={{
        backgroundImage: `url(${backgroundImage})`,
        height: "100vh",
        p: 3,
        boxSizing: "border-box",
      }}
    >
      <Box>
        <Typography
          component="h1"
          sx={{
            color: "#FFFF",
            fontSize: "40px",
            fontWeight: "700",
            margin: "0",
          }}
        >
          Entraide.
        </Typography>
        <Typography
          sx={{
            color: "#3F3979",
            fontSize: "40px",
            fontWeight: "700",
            margin: "0",
          }}
        >
          Partage.
        </Typography>
        <Typography
          sx={{
            color: "#3F3979",
            fontSize: "40px",
            fontWeight: "700",
            margin: "0",
          }}
        >
          Voyage.
        </Typography>
      </Box>

      <Button
        variant="contained"
        sx={{
          borderRadius: 2,
          backgroundColor: "#EBA701",
          py: 1.5,
          color: "white",
          fontSize: "20px",
          fontWeight: "400",
          boxShadow: "0 2px 5px 0 rgba(0, 0, 0, 0.2)",
        }}
        onClick={map}
      >
        Explorer
      </Button>
    </Stack>
  );
};

export default HomePage;
