import React, { useState } from "react";
import Layout from "../../components/Layout/Layout";
import Map from "../../components/Map/Map";
import NavBar from "../../components/NavBar/NavBar";
import RadiusMapDetection from "../../components/RadiusMapDetection/RadiusMapDetection";

const PageMap = () => {
  const [radiusDetection, setRadiusDetection] = useState(5);

  return (
    <>
      <Layout>
        <Map
          radiusDetection={radiusDetection}
          setRadiusDetection={setRadiusDetection}
        />
      </Layout>
      <RadiusMapDetection
        setRadiusDetection={setRadiusDetection}
        radiusDetection={radiusDetection}
      />

      <NavBar></NavBar>
    </>
  );
};

export default PageMap;
