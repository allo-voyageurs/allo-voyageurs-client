import React from "react";
import Layout from "../../components/Layout/Layout";
import PocSeeting from "../../components/Account/PocSeeting";
import NavBar from "../../components/NavBar/NavBar";

const GeolocSeeting = () => {
  return (
    <>
      <Layout>
        <PocSeeting />
      </Layout>
      <NavBar></NavBar>
    </>
  );
};

export default GeolocSeeting;
