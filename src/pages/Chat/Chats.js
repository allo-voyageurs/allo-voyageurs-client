import React from "react";
import Layout from "../../components/Layout/Layout";
import NavBar from "../../components/NavBar/NavBar";
import AllChats from "../../components/Chat/AllChats";
import SearchBar from "../../components/SearchBar/SearchBar";
import Grid from "@mui/material/Grid";


const Chats = () => {
  return (
    <>
      <Layout>
        <Grid container padding="20px">
          <Grid
            item
            xs={12}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div>
              <h2 style={{fontSize: "28px", color:"#8F8FD9", margin: '0', fontWeight: "bold"}}>Chat</h2>
              <p style={{fontSize: "18px", color:"#3F3979", margin: '10px 0 0 0'}}>Retrouves toutes les personnes avec qui tu as pu parler sur l'app !</p>
            </div>
            <SearchBar style={{width: "362px"}}/>
            <AllChats />
          </Grid>
        </Grid>
      </Layout>
      <NavBar />
    </>
  );
};

export default Chats;
