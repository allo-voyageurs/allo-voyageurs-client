import { createTheme } from "@mui/material/styles";

const themeBasis = createTheme({
  palette: {
    primary: {
      main: "#3F3979",
      highlight: "#FFFF",
    },
    secondary: {
      main: "#EBA701",
    },
  },
});

const theme = createTheme(themeBasis, {
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
            
        },
      },
    },
  },
});

export default theme;
